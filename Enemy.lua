local actor = require("Actor")

local enemy = actor.new(0, 20)

local round = function(num, numDecimalPlaces)
    local mult = 10^(numDecimalPlaces or 0)
    return math.floor(num * mult + 0.5) / mult
end

local update = function(self,dt)
    self.counterdt = self.counterdt + dt
    self.firepower = self.firepower + 1

    if round(self.counterdt, 0) % 2 == 0 then
        local random = love.math.random(0,1)
        if random == 0 then
            self.direction = 'right'
        else 
            self.direction = 'left'
        end

        self.counterdt = 1
    end

    if self.direction == 'left' then
        if self.x > 0 then
            self.x = self.x - (self.speed*dt)
        end
    elseif self.x < (windowWidth - self.image:getWidth()) then
        self.x = self.x + (self.speed*dt)
    end    
    
    if self.firepower == 50 then
        self:shoot()
    end

end

local new = function(gameObject)
    return setmetatable(
    {
        image = gameObject.enemyImage,
        x = love.math.random(0,windowWidth - gameObject.enemyImage:getWidth()),
        counterdt = 0,
        name = 'enemy',
        id = love.math.random(0,100),
        bulletColor = 'red',
        state = 'instantiated',
        direction = 'right',

        update = update,
        draw = draw,
    }, 
    {__index = enemy})
end

return {new = new}
