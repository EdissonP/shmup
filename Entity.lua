local Entity = {
    x = 0,
    y = 0,
    speed = 200,
    image = nil,
    width = 0,
    height = 0,
    id = 'entity',
    state = 'undefined',
    name = 'undefined'
}

local draw = function(self)
    love.graphics.draw(self.image.image, self.x, self.y)
end

local update = function(self,dt)

end

local new = function()
    return setmetatable(
    {
        update = update,
        draw = draw,
    }, 
    {__index = Entity})
end

return {new = new}
