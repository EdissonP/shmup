function love.conf(t)
    t.title = "Scrolling Shooter Tutorial"
    t.version = "11.1"
    t.window.width = 480
    t.window.height = 720
    t.window.resizable = false

    t.accelerometerjoystick = false
    t.externalstorage = false

    t.modules.joystick = false
    t.modules.physics = false
    t.modules.audio = false
    t.modules.data = false
    t.modules.event = true
    t.modules.font = true
    t.modules.graphics = true
    t.modules.image = true
    t.modules.keyboard = true
    t.modules.math = true
    t.modules.mouse = false
    t.modules.sound = false
    t.modules.system = false
    t.modules.thread = false
    t.modules.timer = true
    t.modules.touch = true
    t.modules.video = false
    t.modules.window = true
end
