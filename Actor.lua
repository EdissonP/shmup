local entity = require("Entity")
local Actor = entity.new()

local shoot = function(self)
    self.firepower = 0
    self.fire = true
end

local new = function(x, y)
    return setmetatable(
    {
        x = x, 
        y = y,
        firepower = 0,
        fire = false,

        shoot = shoot,
    }, 
    
    {__index = Actor})
end

return {new = new}
