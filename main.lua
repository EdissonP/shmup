local EnemiesHandler = require("EnemiesHandler")
local Collide = require("Collide")

local Player = require("Player") 
local player = Player.new()

local Graphics = require("Graphics")

local game = {
    enemiesNumber = 5,
    state = 'init',
    enemyImage = nil,
    bulletRed = nil,
    bulletBlue = nil,
    enemiesList = {},
    bulletList = {},

    BulletHandler = require("BulletHandler")
}

local FPS = 40

local lag = 0.0
local controlMS = (1000 / FPS) / 1000
local framesCounter = 0
local counterFPS = 0
local timeCounter = 0
local updateTime = 0

local previous =  love.timer.getTime()
local current = 0
local elapsed = 0

local lastTime = love.timer.getTime()
local drawTime = 0
local drawFPS = 0
local drawCounterFPS = 0

local testTime = 0

function love.load(arg)
    game.state = 'loading'

    windowWidth = love.graphics.getWidth()
    windowHeight = love.graphics.getHeight()

    player.image = Graphics.new("plane.png")
    game.enemyImage = Graphics.new("enemy.png")
    game.bulletRed = Graphics.new("bulletRed.png")
    game.bulletBlue = Graphics.new("bullet2.png") 

    EnemiesHandler.createEnemies(game)

    game.state = 'started'
end

function love.update(dt)
    current = love.timer.getTime()
    elapsed = current - previous
    previous = current
    lag = lag + elapsed
    timeCounter = timeCounter + elapsed

    while (lag >= controlMS) do
        player:update(dt)
        
        EnemiesHandler.update(game,dt)

        game.BulletHandler.update(game.bulletList,dt)
        game.BulletHandler.update(player.bullets,dt)

        if player.fire == true then
            table.insert(player.bullets,game.BulletHandler.createBullet(game, player))
            player.fire = false
        end
        
        if #player.bullets ~= 0 then
            -- Control Flow : if no enemies no enter in the first for
            for i,e in pairs(game.enemiesList) do
                -- print ("Enter for enemies")
                for index,b in ipairs(player.bullets) do
                    local isColliding = Collide.checkCollision(e, b)
                    --
                    if isColliding == true then
                        -- table.remove(game.enemiesList, i)
                        -- table.remove(player.bullets, index)
                        e.state = 'delete'
                        b.state = 'delete'

                    end

                end
            end

            for index, bullet in ipairs(player.bullets) do
                if bullet.state == 'delete' then
                    table.remove(player.bullets, index)
                end
            end
        end

        for index, bullet in ipairs(game.bulletList) do
            if bullet.state == 'delete' then
                table.remove(game.bulletList, index)
            end
        end

        lag = lag - controlMS

        framesCounter = framesCounter + 1 
    end

    if timeCounter >= 1 then
        updateTime = dt
        counterFPS = framesCounter
        timeCounter = 0
        framesCounter = 0
        -- love.event.quit()
        -- print("Lag in update " .. lag)
    end

    -- updatelastTime = current

    -- print("Enemies: " .. #game.enemiesList .. " Bullet Player: " .. #player.bullets .. " Bullet Enemy: " .. #game.bulletList .. " Window: " .. windowHeight .." " .. windowWidth)
end

local counter = 0
function love.draw()
    drawCounterFPS = drawCounterFPS + 1
    local current = love.timer.getTime()
    local deltaDraw = current - lastTime
    drawTime = drawTime + deltaDraw

    if drawTime >= 1 then
        drawTime = 0
        drawFPS = drawCounterFPS
        drawCounterFPS = 0
    end
    
    player:draw()
    
    if #game.enemiesList ~= 0 then
        for _,enemy in pairs(game.enemiesList) do
            enemy:draw()
        end
    end
    --
    if #player.bullets ~= 0 then
        for _,bullet in pairs(player.bullets) do
            bullet:draw();
        end
    end
    --
    if #game.bulletList ~= 0 then
        for _,bullet in pairs(game.bulletList) do
            bullet:draw();
        end
    end

    love.graphics.print("Love FPS " .. tostring(love.timer.getFPS()), 10, 10, 0, 1.2, 1.2)
    love.graphics.print("Update FPS " .. counterFPS, 10, 30, 0, 1.2, 1.2)
    love.graphics.print("Draw FPS " .. drawFPS, 10, 50, 0, 1.2, 1.2)
    love.graphics.print("UT " .. updateTime * 1000, 200, 10, 0, 1.2, 1.2)
    love.graphics.print("DT " .. deltaDraw * 1000, 200, 30, 0, 1.2, 1.2)

    lastTime = current

    --TODO: better timestep render loop 
    love.timer.sleep(current + controlMS - love.timer.getTime())
end

function love.mousepressed(x,y,button,istouch)
	-- if istouch then
	-- 	player.x = x - (player.width / 2)
	-- 	player.y = y - (player.height / 2)
	-- end

	--if button == 1 then
	--	player.x = x - (player.width / 2)
	--	player.y = y - (player.height / 2)
	--end
end

function love.keypressed(key)
    if key == "escape" then
        love.event.quit()
    end
end
