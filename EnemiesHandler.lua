local Enemy = require("Enemy")

local createEnemies = function(gameObject)
    for i = 1, gameObject.enemiesNumber, 1 do
        -- TODO: OOP Closure implement protected table 
        table.insert(gameObject.enemiesList, Enemy.new(gameObject))
    end
end

local update = function(gameObject,dt)
    if #gameObject.enemiesList ~= 0 then
        for index,enemy in pairs(gameObject.enemiesList) do
            -- print("Enemies: " .. #gameObject.enemiesList .. " state: " .. enemy.state)
            enemy:update(dt)
            if enemy.state == 'delete' then
                table.remove(gameObject.enemiesList, index)
            end

            if enemy.fire == true then
                table.insert(gameObject.bulletList,gameObject.BulletHandler.createBullet(gameObject, enemy))
                enemy.fire = false
            end
        end
    else
        createEnemies(gameObject)
    end
end

return {
    update = update,
    createEnemies = createEnemies,
}
