local entity = require("Entity")

local bullet = entity.new()

-- local udpdate = function(self)
--
-- end
--
-- local draw = function(self)
--
-- end

local new = function(x, y, source, origin)
    return setmetatable(
    {
        x = x,
        y = y,
        id = 'bullet',
        name = 'bullet',
        image = source,
        origin = origin, 
        state = 'instantiated',

        -- update = update,
        -- draw = draw,
    }, 
    {__index = bullet})
end

return {new = new}
