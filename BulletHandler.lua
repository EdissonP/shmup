local Bullet = require("Bullet")

update = function(table, dt)
	if #table ~= 0 then
	    for index,bullet in ipairs(table) do
    
	        -- print("Bullets: " .. #table .. " state: " .. bullet.state)
	        if bullet.origin == 'player' then
	            bullet.y = bullet.y - (250 * dt)
	        else
	            bullet.y = bullet.y + (250 * dt)
	        end

	        -- print("Bullet : " .. bullet.origin .. " State: " .. bullet.state)

            -- if bullet.y < 0 or bullet.y > windowHeight or bullet.state == 'delete' then
            if bullet.y < 0 or bullet.y > windowHeight then
                bullet.state = 'delete'
                -- print(#table)
                -- table.remove(table, index)
            end
	    end
	end
end

local createBullet = function(gameObject,actor)
    if actor.name == 'player' then
        return Bullet.new(actor.x + (actor.image:getWidth() / 2 - 5), actor.y - (actor.image:getHeight() / 2) - 5, gameObject.bulletBlue, actor.name) 
    else
        return Bullet.new(actor.x + (actor.image:getWidth() / 2 - 5), actor.y - (actor.image:getHeight() / 2) + 100, gameObject.bulletRed, actor.name) 
    end
end

return {
    update = update,
    createBullet = createBullet,
}
