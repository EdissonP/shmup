local Image = {}

local getWidth = function(self)
    return self.image:getWidth()
end

local getHeight = function(self)
    return self.image:getWidth()
end


local new = function(source)
    return setmetatable(
    {
        image = love.graphics.newImage('assets/images/' .. source),
        getWidth = getWidth,
        getHeight = getHeight,

    }, {__index = Image})
end

return {new = new}
