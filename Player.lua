local actor = require("Actor")

local player = actor.new(0, 600)

local update = function(self,dt)
    self.firepower = self.firepower + 1
    
    if love.keyboard.isDown('left') then
        if self.x > 0 then
            self.x = self.x - (self.speed*dt)
        end
    elseif love.keyboard.isDown('right') then
        if self.x < (windowWidth - self.image:getWidth()) then
            self.x = self.x + (self.speed*dt)
        end
    end

    if love.keyboard.isDown('space') and self.firepower >= 50 then
        self:shoot()
    end

end

local new = function()
    return setmetatable(
    {
        name = 'player',
        update = update,
        bullets = {},
    }, 
    {__index = player})
end

return {new = new}
