local testCollision = function(rect1, rect2)
    return rect1.x <= rect2.x+rect2.width
        and rect2.x <= rect1.x+rect1.width
        and rect1.y <= rect2.y + rect2.height
        and rect2.y <= rect1.y + rect1.height
end

local checkCollision = function(entity1, entity2)

    local rect1 = {
        x = entity1.x - entity1.image:getWidth() / 2,
        y = entity1.y - entity1.image:getHeight() / 2,
        width = entity1.image:getWidth(),
        height = entity1.image:getHeight()
    }

    local rect2 = {
        x = entity2.x - entity2.image:getWidth() / 2,
        y = entity2.y - entity2.image:getHeight() / 2,
        width = entity2.image:getWidth(),
        height = entity2.image:getHeight()
    }

    return testCollision(rect1, rect2)
end

return {checkCollision = checkCollision}
